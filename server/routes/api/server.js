
const express = require("express");
const router = express.Router();

//retreiving all the data from the database
router.get("/all", async (req, res) => {
    try {
        const result = await db.any("select * from weather");
        res.status(200).json({
            status: 200,
            data: result,
            message: "Retreived all data related to weather"
        })
    }
    catch (err) {
        console.log(err);
        res.status(400).json({ status: 400, message: "server error" })
    };
})

module.exports = router;