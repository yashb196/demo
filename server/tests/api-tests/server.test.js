const request = require("supertest");
const app = require("../../index");
describe("Testing Server Api", () => {
    it("should return a status code of 200 and provide retreived all data successfully message", done => {
        request(app)
            .get("/api/server/all")
            .then(response => {
                id = response.body.data.id;
                expect(response.statusCode).toBe(200);
                expect(response.body).toEqual(expect.any(Object));
                expect(response.body.data).toEqual(expect.any(Array));
                expect(response.body.message).toBe(
                    "Retreived all data successfully"
                );
                done();
            });
    });

})