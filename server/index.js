const express = require("express");
const server = require("./routes/api/server");
const cors = require("cors");
const logger = require("morgan");
// const config = require("config");
const bodyParser = require("body-parser");
const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(logger("common"));
app.use("/api/server", server);
const port = process.env.port || 5000;

if (process.env.NODE_ENV !== "test")
    app.listen(port, () => console.log(`server is listening on port ${port}`));
module.exports = app;